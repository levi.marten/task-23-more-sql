SELECT GROUP_CONCAT(language) AS Languages, country.name 
FROM countrylanguage 
LEFT JOIN country
ON countrylanguage.countrycode = country.Code
GROUP BY country.name

/* Select all the languages per country, All the languages should be comma separated in 1 column and country names should only appear once (No duplicate country names) */