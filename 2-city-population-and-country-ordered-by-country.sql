SELECT city.name, city.Population, country.name AS country 
FROM city 
LEFT JOIN country ON city.CountryCode = country.Code 
ORDER BY country;